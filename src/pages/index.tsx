import type { NextPage } from 'next'
import Head from 'next/head'
import { Footer } from '../Components/Footer'
import { Header } from '../Components/Header'
import { InfoPag } from '../Components/PagInfo'
import { Container } from '../styles/pagesStyles/Home'

const Home: NextPage = () => {
  return (
    <>
      <Header />
      <Container>
        <Head>
          <title>ABAMS-ILPs</title>
        </Head>
          <InfoPag></InfoPag>
        <Footer />

        {/* {abamsInfo.map(item => {
          return (
            <Dashboard
              key={item.id}
              name={item.name}
              description={item.description}
              address={item.address}
              telephone={item.telephone}
              imageUrl={item.imageUrl}
              number={item.number}
            />
          )
        })} */}
      </Container>
    </>
  )
}

export default Home
