import { Container, Table } from './styles'

export function InfoPag() {
  return (
    <Container>
      <Table>
        <thead>
          <tr>
            <td>Cod Solicitação</td>
            <td>Nome ILPI</td>
            <td>Telefone</td>
            <td>Data Solicitação</td>
            <td>Status</td>
            <td>Sobre</td>
            <td>Situação</td>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>1</td>
            <td>Mark</td>
            <td>539919191</td>
            <td>24/02/2022</td>
            <td>Pendente</td>
            <td>iojsoivcajoijd cvaiopcjouapjciaj cvojai oc jacjac</td>
            <td>
            <button>Aprovar</button>
              <button>Reprovar</button>
            </td>
          </tr>
          <tr>
            <td>2</td>
            <td>Mark</td>
            <td>539919191</td>
            <td>24/02/2022</td>
            <td>Pendente</td>
            <td>iojsoivcajoijd cvaiopcjouapjciaj cvojai oc jacjac</td>
            <td>
              <button>Aprovar</button>
              <button>Reprovar</button>
            </td>
          </tr>
          <tr>
            <td>3</td>
            <td>Mark</td>
            <td>539919191</td>
            <td>24/02/2022</td>
            <td>Pendente</td>
            <td>iojsoivcajoijd cvaiopcjouapjciaj cvojai oc jacjac</td>
            <td>
            <button>Aprovar</button>
              <button>Reprovar</button>
            </td>
          </tr>
        </tbody>
      </Table>
    </Container>
  )
}
