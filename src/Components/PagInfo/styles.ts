import styled from 'styled-components'

export const Table = styled.div`
  width: 100%;
  color: black;
  border: 1px solid #b9b9b9;
  padding: 0px;
  margin-top: 50px;
  margin-bottom: 50px;
  thead {
    width: 100%;
    height: 35px;
    background-color: rgba(0, 0, 0, 0.05);;
    font-Weight: bold;
  }
  tbody {
    height: 50px;
    border: 1px solid #b9b9b9;
  }
  tr {
    text-align: center;
    gap: 30px;
  }
  td {
    padding: 10px;
    border: 1px solid #b9b9b9;
  }
  button {
    gap: 10px;
    padding: 10px;
  }
  
`

export const Container = styled.div``
