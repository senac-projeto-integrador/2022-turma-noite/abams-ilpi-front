import styled from 'styled-components'

export const Container = styled.div`
  width: 100%;
  height: 138px;
  background-color: ${({ theme }) => theme.colors.primary};

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  img {
    width: 209px;
    height: 82px;

    margin-top: 10px;
  }

  span {
    color: black;
    margin-bottom: 10px;
  }
`
