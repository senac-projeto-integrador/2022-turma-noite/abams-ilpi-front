import styled from 'styled-components'
import Logo from '../../assets/logoImage.svg'
import { Container } from './styles'

export const Footer = () => {
  return (
    <Container>
      <img src={Logo} alt="Logo" />
      <span>Direitos autorais 2022 ABAMS | Todos os direitos reservados.</span>
    </Container>
  )
}
