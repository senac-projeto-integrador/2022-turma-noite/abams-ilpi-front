import Logo from '../../assets/logoImage.svg'
import { Container } from './styles'

export function Header() {
  return (
    <Container>
      <img src={Logo as any} alt="Logo" />

      <nav>
        <button>
          <a href="">Pagina Inicial</a>
        </button>
        <button>
          <a href="">IlPI</a>
        </button>
        <button>
          <a href="">Login</a>
        </button>
      </nav>
    </Container>
  )
}
