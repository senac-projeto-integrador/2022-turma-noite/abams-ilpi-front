import styled from 'styled-components'

export const Container = styled.div`
  height: 6rem;
  /* border-bottom: 1px solid #8a8a8a; */
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  /* justify-content: space-between; */
  background: ${props => props.theme.colors.backgroundColor_secondary};

  img {
    margin-top: 1rem;
    height: 15rem;
    width: 15rem;
  }

  nav {
    display: flex;
    gap: 2.5rem;
    justify-content: flex-start;
  }

  button {
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 1rem;
    background-color: #60c44c;
    height: 3.5rem;
    width: 7rem;
    border-style: hidden;

    a {
      color: #fff;
      display: inline-block;
      position: relative;
      padding: 0 0.5rem;
      height: 5rem;
      line-height: 5rem;
      text-decoration: none;

      transition: color 0.2s;

      a:hover {
        text-decoration: underline;
        color: #220f0f;
      }
    }
  }
`
