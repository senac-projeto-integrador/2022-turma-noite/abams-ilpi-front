const theme = {
  colors: {
    backgroundColor_primary: '#F3F3F3',
    backgroundColor_secondary: '#ECECEC',
    textColor: '#e1e1e6',
    primary: '#60C44C',

    baby_pink: '#F0CAC1FF'
  },

  fonts: {
    inter_primary_400: 'inter_300',
    inter_secondary_700: 'inter_700',
    inter_bold_900: 'inter_900',

    roboto_300: 'roboto_300',
    roboto_400: 'roboto_400',
    roboto_500: 'roboto_500'
  }
}

export default theme
